package com.luxoft.sqedu.javalang;

public class LinkedList implements List {

	private Node head;
	private int size;

	public LinkedList() {
		head = new Node(null);
		size = 0;
	}

	@Override
	public Object set(int index, Object dataValue) {
		checkIndex(index);

		Node current = getNode(index);
		Object oldValue = current.getData();
		current.setData(dataValue);

		return oldValue;
	}

	@Override
	public Object get(int index) {
		checkIndex(index);
		return getNode(index).getData();
	}

	private Node getNode(int index) {
		Node current = head;
		for (int i = 0; i < index; i++) {
			current = current.getLink();
		}
		return current;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean add(Object dataValue) {

		if (size == 0) {
			add(0, dataValue);
		} else {
			add(size, dataValue);
		}

		return true;
	}

	@Override
	public boolean add(int index, Object dataValue) {
		checkIndex(index);

		if (size == 0) {
			set(0, dataValue);

		} else {
			if (index == 0) {
				Node current = getNode(index);
				Node newNode = new Node(dataValue, current);
				head = newNode;
			} else {
				Node prevNode = getNode(index - 1);
				Node current = prevNode.getLink();
				Node newNode = new Node(dataValue, current);
				prevNode.setLink(newNode);
			}
		}

		size++;
		return true;
	}

	@Override
	public Object remove(int index) {
		checkEndIndex(index);

		if (index == 0) {
			Node nextNode = getNode(index + 1);

			if (nextNode == null) {
				head = new Node(null);
			} else {
				head = nextNode;
			}
		} else {
			Node prevNode = getNode(index - 1);
			prevNode.setLink(prevNode.getLink().getLink());
		}
		size--;
		return true;
	}

	@Override
	public boolean remove(Object dataValue) {
		int index = -1;

		Node current = head;
		for (int i = 0; i < size(); i++) {
			if (dataValue.equals(current.getData())) {
				index = i;
				break;
			}
			current = current.getLink();
		}

		if (index != -1) {
			remove(index);
			return true;
		} else {
			return false;
		}
	}

	private void checkIndex(int index) {
		if (index < 0 || index > size) {
			throw new IndexOutOfBoundsException("Invalid LinkedList index " + index + "\n Size of LinkedList: " + size + "\n ");
		}
	}

	private void checkEndIndex(int index) {
		if (index >= size()) {
			throw new IndexOutOfBoundsException("Invalid LinkedList end index " + index + "\n Size of LinkedList: " + size + "\n ");
		}
	}

	public void printLinkedList( ) {
		LinkedList list=this;
		for (int i = 0; i < size; i++) {
			System.out.println(String.valueOf("index " + i + " element " + list.get(i) + "\n"));
		}
		System.out.println(String.valueOf("size of list " + size()));

	}

	private class Node {
		Node next = null;
		Object data;

		public Node(Object dataValue) {
			next = null;
			data = dataValue;
		}

		public Node(Object dataValue, Node nextValue) {
			data = dataValue;
			next = nextValue;
		}

		public void setLink(Node nextValue) {
			next = nextValue;
		}

		public void setData(Object dataValue) {
			data = dataValue;
		}

		public Node getLink() {
			return next;
		}

		public Object getData() {
			return data;
		}

	}
}