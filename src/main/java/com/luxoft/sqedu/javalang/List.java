package com.luxoft.sqedu.javalang;

public interface List {

	/* set item at given position. */
	public Object set(int index, Object obj);

	/* get item from given position.*/
	public Object get(int index);

	/* return true if this list is empty.*/
	public boolean isEmpty();

	/* return number of elements in this list*/
	public int size();

	/* add item at end of list.*/
	public boolean add(Object obj);

	/* add obj at given position. */
	public boolean add(int index, Object obj);

	/* return and remove element from given position*/
	public Object remove(int index);

	/* remove first copy of obj from this list. */
	public boolean remove(Object obj);

}
