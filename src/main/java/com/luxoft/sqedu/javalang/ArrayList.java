package com.luxoft.sqedu.javalang;

public class ArrayList implements List {

	private int amount = 0; //elements[0..amount-1]
	private Object[] elements;
	private static final int volume = 20;

	public ArrayList() {
		elements = new Object[volume];
		amount = 0;
	}

	public Object set(int index, Object obj) {
		Object result = elements[index];
		elements[index] = obj;
		return result;
	}

	public int size() {
		return amount;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public Object get(int index) {
		return elements[index];
	}

	private void ensureCapacity(int minCapacity) {
		if (minCapacity > elements.length) {
			Object[] new_elements = new Object[Math.max((elements.length) * 2, minCapacity)];
			for (int k = 0; k < size(); k++) {
				new_elements[k] = elements[k];
			}
			elements = new_elements;
		}
	}

	public boolean add(Object obj) {
		ensureCapacity(size() + 1);
		elements[amount] = obj;
		amount++;
		return true;
	}

	public boolean add(int index, Object obj) {
		checkIndex(index);
		ensureCapacity(size() + 1);
		for (int i = size(); i > index; i--) {
			elements[i] = elements[i - 1];
		}
		elements[index] = obj;
		amount++;
		return true;
	}

	public Object remove(int index) {
		checkEndIndex(index);
		Object result = elements[index];
		for (int i = index; i < size() - 1; i++) {
			elements[i] = elements[i + 1];
		}
		amount--;
		elements[amount] = null;
		return result;
	}

	public boolean remove(Object obj) {
		int index = -1;

		if (obj == null) {
			for (int i = 0; i < size(); i++) {
				if (elements[i] == null) {
					index = i;
					break;
				}
			}
		} else {
			for (int i = 0; i < size(); i++) {
				if (obj.equals(elements[i])) {
					index = i;
					break;
				}
			}
		}

		if (index != -1) {
			remove(index);
			return true;
		} else {
			return false;
		}
	}

	private void checkIndex(int index) {
		if (index > size() || index < 0) {
			throw new IndexOutOfBoundsException("Invalid ArrayList index " + index + "\n Size of ArrayList: " + size());
		}
	}

	private void checkEndIndex(int index) {
		if (index >= size()) {
			throw new IndexOutOfBoundsException("Invalid ArrayList end index " + index + "\n Size of ArrayList: " + size() + "\n ");
		}
	}

	public void printArrayList(ArrayList list) {
		for (int i = 0; i < size(); i++) {
			System.out.println(String.valueOf("index " + i + " element " + elements[i]));
		}

	}

}