package com.luxoft.sqedu.javalang;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LinkedListTest {

	LinkedList list;

	@Before
	public void doBefore() {
		list = new LinkedList();
	}

	@After
	public void doAfter() {
	}

	@Test
	public void testIsEmpty() {
		Assert.assertTrue("list should be empty", list.isEmpty());
	}

	@Test
	public void testSize() {
		Object obj = new Object();
		for (int i = 0; i < 30; i++) {
			list.add(obj);
		}

		Assert.assertEquals("Size of list after adding 30 elements", 30, list.size());
	}

	@Test
	public void testSet() {
		Object obj0 = new Object();
		Object obj1 = new Object();
		Object new_obj = new Object();

		list.add(obj0);
		list.add(obj1);
		list.set(0, new_obj);

		Assert.assertEquals("Element is replaced", new_obj, list.get(0));
		Assert.assertEquals("Next element remains unchanged", obj1, list.get(1));
		Assert.assertEquals("Size of list is unchanged", 2, list.size());
	}

	@Test
	public void testGet() {
		Object obj = new Object();
		Object obj1 = new Object();
		Object obj2 = new Object();
		list.add(obj);
		list.add(obj1);
		list.add(obj2);

		Assert.assertEquals("Size of list after adding 3 elements", 3, list.size());
		Assert.assertEquals("First element getting from 0 position", obj, list.get(0));
		Assert.assertEquals("Third  element getting from 2 position", obj2, list.get(2));
		Assert.assertEquals("Second element getting from 1 position", obj1, list.get(1));
	}

	@Test
	public void testAddByIndex() {
		Object obj0 = new Integer(1);
		Object obj1 = new Integer(2);
		Object obj2 = new Integer(3);

		list.add(0, obj0);
		Assert.assertEquals("First element was added by index 0", obj0, list.get(0));
		Assert.assertEquals("Size of list is 1", 1, list.size());

		list.add(1, obj1);
		Assert.assertEquals("First element remains unchanged", obj0, list.get(0));
		Assert.assertEquals("Second element wasn't added by index 1", obj1, list.get(1));
		Assert.assertEquals("List size increases at 1, actual size 2", 2, list.size());

		list.add(0, obj2);
		Assert.assertEquals("First element remains unchanged", obj2, list.get(0));
		Assert.assertEquals("Third element wasn't added by index 1", obj0, list.get(1));
		Assert.assertEquals("Second element moves on one position forward, on third position", obj1, list.get(2));
		Assert.assertEquals("Size after adding three elements", 3, list.size());
	}

	@Test
	public void testAdd() {
		Object obj0 = new Object();
		Object obj1 = new Object();
		list.add(obj0);
		list.add(obj1);

		Assert.assertEquals("Size of list after adding two elements", 2, list.size());
		Assert.assertEquals("First element added on the zero position", obj0, list.get(0));
		Assert.assertEquals("Second element added on next, first position", obj1, list.get(1));
	}

	@Test
	public void testRemoveByIndex() {
		Object obj0 = new Object();
		Object obj1 = new Object();
		Object obj2 = new Object();
		list.add(0, obj0);
		list.add(1, obj1);
		list.add(2, obj2);

		Assert.assertEquals("Size of list after adding 3 elements", 3, list.size());
		list.remove(1);
		Assert.assertEquals("List size has decreased by one, 2 elements left", 2, list.size());
		Assert.assertEquals("Previous element remains unchanged", obj0, list.get(0));
		Assert.assertEquals("Next element moves on deleted position 1", obj2, list.get(1));
		list.remove(0);
		Assert.assertEquals("List size has decreased by one, 1 element left", 1, list.size());
		Assert.assertEquals("Next element moves on deleted position 0", obj2, list.get(0));
		list.remove(0);
		Assert.assertEquals("List size has decreased by one, no elements left", 0, list.size());
	}

	@Test
	public void testRemove() {
		Object obj0 = new Object();
		Object obj1 = new Object();
		Object obj2 = new Object();
		list.add(obj0);
		list.add(obj1);
		list.add(obj2);

		Assert.assertEquals("Size of list after adding 3 elements", 3, list.size());
		list.remove(obj1);
		Assert.assertEquals("List size has decreased by one, 2 elements left", 2, list.size());
		Assert.assertEquals("Next element moves on deleted position 1", obj2, list.get(1));
		Assert.assertEquals("Previous element remains unchanged", obj0, list.get(0));
		list.remove(obj0);
		Assert.assertEquals("list size has decreased by one, 1 element left", 1, list.size());
		Assert.assertEquals("Next element moves on deleted position 0", obj2, list.get(0));
		list.remove(obj2);
		Assert.assertEquals("List size has decreased by one, no elements left", 0, list.size());
	}

	@Test
	public void testRemoveFirst() {
		Object obj0 = new Object();
		Object obj1 = new Object();
		Object obj2 = new Object();

		list.add(obj0);
		list.remove(obj0);
		list.add(obj1);

		Assert.assertEquals(1, list.size());
		Assert.assertEquals(obj1, list.get(0));
	}

}
